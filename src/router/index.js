import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: () => import('@/pages/login/index.vue')
  },
  {
    path: '/direct-login',
    name: 'DirectLogin',
    component: () => import('@/pages/login/direct.vue')
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('@/pages/register/index.vue')
  },
  {
    path: '/register/country',
    name: 'Country',
    component: () => import('@/pages/register/country.vue')
  },
  {
    path: '/register/form',
    name: 'RegisterForm',
    component: () => import('@/pages/register/form.vue')
  },
  {
    path: '/register/otp',
    name: 'RegisterOtp',
    component: () => import('@/pages/register/otp.vue')
  },
  {
    path: '/pin/create/:access',
    name: 'PinCreate',
    component: () => import('@/pages/pin/create.vue')
  },
  {
    path: '/pin/confirmation',
    name: 'PinConfirmation',
    component: () => import('@/pages/pin/confirmation.vue')
  },
  {
    path: '/pin/check/:access',
    name: 'PinCheck',
    component: () => import('@/pages/pin/check.vue')
  },
  {
    path: '/pin/lock',
    name: 'PinLock',
    component: () => import('@/pages/pin/lock.vue')
  },
  {
    path: '/pin/forget/otp/phone',
    name: 'PinForgetOtp',
    component: () => import('@/pages/pin/forget/otp.vue')
  },
  {
    path: '/pin/forget/otp/email',
    name: 'PinForgetEmail',
    component: () => import('@/pages/pin/forget/otpEmail.vue')
  },
  {
    path: '/login/otp',
    name: 'LoginOtp',
    component: () => import('@/pages/login/otp.vue')
  },
  {
    path: '/login/email',
    name: 'LoginEmail',
    component: () => import('@/pages/login/email/index.vue')
  },
  {
    path: '/password/verification',
    name: 'PasswordVerification',
    component: () => import('@/pages/password/verification.vue')
  },
  {
    path: '/email/verification_number',
    name: 'VerificationNumber',
    component: () => import('@/pages/login/email/verificationNumber.vue')
  },
  {
    path: '/login/email/otp',
    name: 'LoginEmailOtp',
    component: () => import('@/pages/login/email/otp.vue')
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: () => import('@/pages/dashboard.vue')
  },
  {
    path: '/password/forget/otp',
    name: 'PasswordOtp',
    component: () => import('@/pages/password/forget/otp.vue')
  },
  {
    path: '/password/forget/form',
    name: 'PasswordForm',
    component: () => import('@/pages/password/forget/form.vue')
  },
  {
    path: '/password/forget/completed',
    name: 'PasswordCompleted',
    component: () => import('@/pages/password/forget/completed.vue')
  },
  // data email
  {
    path: '/email/form',
    name: 'FormEmail',
    component: () => import('@/pages/email/form.vue')
  },
  {
    path: '/email/confirmation',
    name: 'EmailConfirmation',
    component: () => import('@/pages/email/confirmation.vue')
  },
  {
    path: '/email/otp',
    name: 'EmailOtp',
    component: () => import('@/pages/email/otpEmail.vue')
  },
  // change number
  {
    path: '/change/number',
    name: 'ChangeNumber',
    component: () => import('@/pages/change/number/index.vue')
  },
  {
    path: '/change/number/otp_email',
    name: 'ChangeNumberOtpEmail',
    component: () => import('@/pages/change/number/otpEmail.vue')
  },
  {
    path: '/change/number/form',
    name: 'FormChangeNumber',
    component: () => import('@/pages/change/number/form.vue')
  },
  {
    path: '/change/number/otp',
    name: 'ChangeNumberOtp',
    component: () => import('@/pages/change/number/otp.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
