import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import ReusableDialogs from '@/components/reusable/ReusableDialogs'
import ReusableLoading from '@/components/reusable/ReusableLoading'
import ReusableToolbar from '@/components/reusable/ReusableToolbar'
import ReusableBottomSheet from '@/components/reusable/ReusableBottomSheet'
import '@/assets/generic/style/index.scss'
import 'flag-icon-css/css/flag-icon.css'
import OtpInput from '@bachdgvn/vue-otp-input'

// Global Component
Vue.component('reusable-dialogs', ReusableDialogs)
Vue.component('reusable-loading', ReusableLoading)
Vue.component('reusable-toolbar', ReusableToolbar)
Vue.component('reusable-bottom-sheet', ReusableBottomSheet)

// Component OtpInput
Vue.component('v-otp-input', OtpInput)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
