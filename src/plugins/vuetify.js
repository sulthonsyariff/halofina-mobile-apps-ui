import Vue from 'vue'
import Vuetify from 'vuetify/lib'

Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    dark: false,
    themes: {
      light: {
        primary: '#51AAF4',
        secondary: '#8F9CF2',
        success: '#17CF82',
        error: '#F26969',
        warning: '#FFB245',
        info: '#42A5F5'
      }
    }
  }
})
