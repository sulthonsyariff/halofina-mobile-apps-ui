export function pinValidation (numbers) {
  const pin = [...numbers]
  const totalPin = pin.length
  const isArrayValueEqual = pin.every((element) => element === pin[0])
  const isArrayValueAscending = pin
    .slice(1)
    .map((element, index) => element > pin[index] && element - pin[index] === 1)
    .every((element) => element)
  const isArrayValueDescending = pin
    .slice(1)
    .map(
      (element, index) => element < pin[index] && element - pin[index] === -1
    )
    .every((element) => element)
  if (totalPin === 6) {
    if (isArrayValueEqual || isArrayValueAscending || isArrayValueDescending) {
      return true
    } else {
      return false
    }
  }
}
