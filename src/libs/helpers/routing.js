const origin = window.location.origin
const hash = '/#'

export function getOrigin () {
  return origin
}

export function setNamespace (namespace) {
  localStorage.setItem('halofina-namespace', namespace)
}

export function goToNamespace (namespace, route, mode) {
  if (route) {
    if (mode === 'replace') {
      window.location.replace(origin + '/' + namespace + hash + route)
    } else {
      window.location.assign(origin + '/' + namespace + hash + route)
    }
  } else {
    if (mode === 'replace') {
      window.location.replace(origin + '/' + namespace + hash)
    } else {
      window.location.assign(origin + '/' + namespace + hash)
    }
  }
}

export function setRouteToLocalStorage (route) {
  localStorage.setItem('halofina-route', route)
}
