import createPersistedState from 'vuex-persistedstate'
import Vue from 'vue'
import Vuex from 'vuex'

import register from './register'
import login from './login'
import pin from './pin'
import global from './global'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    register,
    login,
    pin,
    global
  },
  plugins: [createPersistedState({
    key: 'account-state'
  })]
})
