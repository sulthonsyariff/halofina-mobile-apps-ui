import Api from '@halofina/vue-library/src/libs/Api'

export default {
  namespaced: true,
  state: {
    oldPin: '',
    pinTemp: '',
    counter: 3,
    access: ''
  },
  mutations: {
    pinTemp (state, pin) {
      state.pinTemp = pin
    },
    oldPin (state, pin) {
      state.oldPin = pin
    },
    counter (state, data) {
      state.counter = data
    },
    access (state, data) {
      state.access = data
    },
    reset (state) {
      state.pinTemp = ''
      state.oldPin = ''
      state.counter = 3
      state.access = ''
    }
  },
  actions: {
    async validationPin ({ commit }, data) {
      const res = await Api('/user/register/valid_pin_format').post(data)

      if (res.success) {
        if (res.data === true) {
          commit('pinTemp', data.pin)
        }
      }

      return res
    },
    async forgetPin ({ commit }, data) {
      const res = await Api('/forgot_pin').post(data)

      return res
    },
    async changePin ({ commit }, data) {
      const res = await Api('/change_pin').post(data)

      return res
    }
  },
  getters: {
    oldPin (state) {
      return state.oldPin
    },
    pinTemp (state) {
      return state.pinTemp
    },
    counter (state) {
      return state.counter
    },
    access (state) {
      return state.access
    }
  }
}
