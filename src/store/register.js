import Api from '@halofina/vue-library/src/libs/Api'

export default {
  namespaced: true,
  state: {
    fullname: '',
    referral: '',
    uuid: ''
  },
  mutations: {
    form (state, data) {
      state.fullname = data.fullname
      state.referral = data.referral
    },
    country (state, data) {
      state.country = data.country
    },
    createTemporary (state, data) {
      state.uuid = data
    },
    reset (state) {
      state.fullname = ''
      state.referral = ''
      state.uuid = ''
      localStorage.removeItem('halofina-token')
      localStorage.removeItem('X-Finarobo-Authorization')
    }
  },
  actions: {
    async country ({ commit }) {
      const res = await Api('/reference/country/get_all').post()

      return res
    },
    async checkPhoneExist ({ commit }, data) {
      const res = await Api('/user/exist_phone').post(data)

      return res
    },
    async createTemporary ({ commit }, data) {
      const res = await Api('/user/register/create').post(data)

      if (res.success) {
        commit('createTemporary', res.data)
      }

      return res
    },
    async limitCountdown ({ commit }, data) {
      const res = await Api('/configuration/get_by_key').post(data)

      return res
    },
    async resendOtp ({ commit }, data) {
      const res = await Api('/user/register/resend_otp').post(data)

      return res
    },
    async verifyOtp ({ commit }, data) {
      const res = await Api('/user/register/verify_otp').post(data)

      return res
    },
    async updateUser ({ commit }, data) {
      const res = await Api('/user/register/update_user').post(data)

      return res
    },
    async registerFirstTime ({ commit }, data) {
      const res = await Api('/register').post(data)

      return res
    },
    async registerPin ({ commit }, data) {
      const res = await Api('/register_pin').post(data)

      return res
    }
  },
  getters: {
    fullname (state) {
      return state.fullname
    },
    referral (state) {
      return state.referral
    },
    uuid (state) {
      return state.uuid
    }
  }
}
