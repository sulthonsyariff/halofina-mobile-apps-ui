import Api from '@halofina/vue-library/src/libs/Api'

export default {
  namespaced: true,
  state: {
    userUuid: ''
  },
  mutations: {
    userUuid (state, data) {
      state.userUuid = data
    },
    reset (state) {
      state.userUuid = ''
      localStorage.removeItem('halofina-token')
      localStorage.removeItem('X-Finarobo-Authorization')
    }
  },
  actions: {
    // login with pin
    async checkPhoneExist ({ commit }, data) {
      const res = await Api('/check_phone').post(data)

      return res
    },
    async existPhoneUsed ({ commit }, data) {
      const res = await Api('/user/exist_used_phone').post(data)

      return res
    },
    async resendOtp ({ commit }, data) {
      const res = await Api('/user/resend_otp').post(data)

      return res
    },
    async resendOtpEmail ({ commit }, data) {
      const res = await Api('/user/resend_otp_email').post(data)

      return res
    },
    async verifyOtp ({ commit }, data) {
      const res = await Api('/user/verify_otp').post(data)

      return res
    },
    async loginPin ({ commit }, data) {
      const res = await Api('/login_pin').post(data)

      return res
    },
    // email otp
    async sendOtpEmail ({ commit }, data) {
      const res = await Api('/user/email_otp').post(data)

      return res
    },
    // login with email
    async checkEmail ({ commit }, data) {
      const res = await Api('/user/check_email').post(data)

      return res
    },
    async checkPassword ({ commit }, data) {
      const res = await Api('/user/check_password').post(data)

      return res
    },
    async setPassword ({ commit }, data) {
      const res = await Api('/user/set_password').post(data)

      return res
    }
  },
  getters: {
    userUuid (state) {
      return state.userUuid
    }
  }
}
