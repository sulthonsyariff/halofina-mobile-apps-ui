import Api from '@halofina/vue-library/src/libs/Api'

export default {
  namespaced: true,
  state: {
    loading: false,
    code: '+62',
    classCode: 'id',
    uuidCountry: '8fe1af05-fc56-4faf-9084-89236d85666f',
    number: '',
    email: '',
    country: []
  },
  mutations: {
    loading (state, data) {
      state.loading = data
    },
    country (state, data) {
      state.country = data
    },
    email (state, data) {
      state.email = data
    },
    code (state, data) {
      state.code = data
    },
    classCode (state, data) {
      state.classCode = data
    },
    uuidCountry (state, data) {
      state.uuidCountry = data
    },
    number (state, data) {
      state.number = data
    },
    otp (state, data) {
      state.otp = data
    },
    reset (state) {
      state.number = ''
      state.email = ''
    }
  },
  actions: {
    async updateUser ({ commit }, data) {
      const res = await Api('/user/update').post(data)

      return res
    },
    async getByUuid ({ commit }, data) {
      const res = await Api('/user/get_by_uuid').post(data)

      return res
    },
    async logout ({ commit }, data) {
      const res = await Api('/logout').post(data)

      if (res.success) {
        localStorage.removeItem('halofina-token')
        localStorage.removeItem('halofina-uuid')
        localStorage.removeItem('X-Finarobo-Authorization')
      }

      return res
    },
    // email access in list kyc
    async checkUsedEmail ({ commit }, data) {
      const res = await Api('/user/check_used_email').post(data)

      return res
    },
    async verifyEmail ({ commit }, data) {
      const res = await Api('/user/verify_email').post(data)

      return res
    },
    async verifyOtpEmail ({ commit }, data) {
      const res = await Api('/user/verify_otp_email').post(data)

      return res
    },
    async verifyPhone ({ commit }, data) {
      const res = await Api('/user/verify_phone').post(data)

      return res
    },
    async verifyOtpPhone ({ commit }, data) {
      const res = await Api('/user/verify_otp_phone').post(data)

      return res
    },
    async checkChangePhone ({ commit }, data) {
      const res = await Api('/user/check_change_phone').post(data)

      return res
    },
    async resendOtpPhone ({ commit }, data) {
      const res = await Api('/user/resend_otp_phone').post(data)

      return res
    }
  },
  getters: {
    loading (state) {
      return state.loading
    },
    country (state) {
      return state.country
    },
    code (state) {
      return state.code
    },
    email (state) {
      return state.email
    },
    classCode (state) {
      return state.classCode
    },
    uuidCountry (state) {
      return state.uuidCountry
    },
    number (state) {
      return state.number
    },
    otp (state) {
      return state.otp
    }
  }
}
